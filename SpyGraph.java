///////////////////////////////////////////////////////////////////////////////
// Title:            (Program 5)
// Files:            Game.java, Player.java, GraphNode.java, Spy.java,
//					 InvalidAreaFileException.java, SpyGraph.java, Neighbor.java, 
//                   Test.javaNotNeighborException.java
// Semester:         (CS 367) Spring 2016
//
// Author:           (Tone Yu, Arpit Agarwal)
// Email:            (tyu44@wisc.edu)
// CS Login:         (tone)
// Lecturer's Name:  (Skrenty)
//
////////////////////////////////////////////////////////////////////////////////
//
// Pair Partner:     (Arpit Agarwal)
// Email:            (agarwal32@wisc.edu)
// CS Login:         (arpit)
// Lecturer's Name:  (Skrenty)
////////////////////////////////////////////////////////////////////////////////
import java.util.*;
/**
 * Stores all vertexes as a list of GraphNodes.  Provides necessary graph operations as
 * need by the SpyGame class.
 * 
 * @author strominger
 *
 */
public class SpyGraph implements Iterable<GraphNode> {

    /** DO NOT EDIT -- USE THIS LIST TO STORE ALL GRAPHNODES */
    private List<GraphNode> vlist;


    /**
     * Initializes an empty list of GraphNode objects
     */
    public SpyGraph(){
         // TODO initialize data member(s)
    	vlist = new ArrayList<GraphNode>();
    }

    /**
     * Add a vertex with this label to the list of vertexes.
     * No duplicate vertex names are allowed.
     * @param name The name of the new GraphNode to create and add to the list.
     */
    public void addGraphNode(String name){
         // TODO implement this method
    	if(!vlist.contains(name)){
    		vlist.add(new GraphNode(name));
    	}
    }

    /**
     * Adds v2 as a neighbor of v1 and adds v1 as a neighbor of v2.
     * Also sets the cost for each neighbor pair.
     *   
     * @param v1name The name of the first vertex of this edge
     * @param v2name The name of second vertex of this edge
     * @param cost The cost of traveling to this edge
     * @throws IllegalArgumentException if the names are the same
     */
    public void addEdge(String v1name, String v2name, int cost) throws IllegalArgumentException{
         // TODO implement this method
    	//if(v1name.equals(v2name) || !(vlist.contains(v2name) && vlist.contains(v1name))) throw new IllegalArgumentException();
    	if(v1name.equals(v2name)) throw new IllegalArgumentException();
    	else{
    		this.getNodeFromName(v1name).addNeighbor(getNodeFromName(v2name), cost);
    		this.getNodeFromName(v2name).addNeighbor(getNodeFromName(v1name), cost);
    	}
    }

    /**
     * Return an iterator through all nodes in the SpyGraph
     * @return iterator through all nodes in alphabetical order.
     */
    public Iterator<GraphNode> iterator() {
        return vlist.iterator();
    }

    /**
     * Return Breadth First Search list of nodes on path 
     * from one Node to another.
     * @param start First node in BFS traversal
     * @param end Last node (match node) in BFS traversal
     * @return The BFS traversal from start to end node.
     */
    public List<Neighbor> BFS( String start, String end ) {
         // TODO implement this method
         // may need and create a companion method
    	Queue<GraphNode> queue = new LinkedList<GraphNode>();
    	List<GraphNode> visitedNodes = new ArrayList<GraphNode>();
    	
    	GraphNode startNode = getNodeFromName(start);
    	GraphNode endNode = getNodeFromName(end);
    	if(endNode==null){
    		System.out.println("Incorrect vertex name");
    		return new ArrayList<Neighbor>();
    	}
    	HashMap<GraphNode, GraphNode> hm = new HashMap<GraphNode, GraphNode>();
    	visitedNodes.add(startNode);
    	hm.put(startNode, startNode);
    	
    	queue.add(startNode);
    	boolean endFound = false;
    	while(!queue.isEmpty() && !endFound){
    		GraphNode current = queue.remove();
    		for(Neighbor k: current.getNeighbors()){
    			GraphNode kNode = k.getNeighborNode();
    			if(!visitedNodes.contains(kNode)){
    				visitedNodes.add(kNode);
    				hm.put(kNode, current);
    				queue.add(kNode);
    				if(kNode.equals(endNode)) {
    					visitedNodes.add(kNode);
    					endFound = true;
    					break;
    				}
    			}
    			if(endFound) break;
    		}
    	}
    	
    	List<GraphNode> backtrack = new ArrayList<GraphNode>();
    	GraphNode curr = endNode;
    	while(!curr.equals(startNode)){
    		backtrack.add(curr);
    		curr = hm.get(curr);
    	}
    	backtrack.add(startNode);
    	Collections.reverse(backtrack);
    	
    	//TYPE PROBLEM
    	List<Neighbor> path = new ArrayList<Neighbor>();
    	GraphNode prevNode = backtrack.get(0);
    	for(GraphNode c: backtrack){
    		//if(c.equals(visitedNodes.get(visitedNodes.size()-1))){}
    		if(c.equals(prevNode)){
    		} else {
    			try{
    				Neighbor neighborNode = new Neighbor(prevNode.getCostTo(c.getNodeName()), c);
    				path.add(neighborNode);
    				prevNode = c;
    			} catch (NotNeighborException e){
    				break;
    			}
    		}
    	}
        return path;
    }


    /**
     * @param name Name corresponding to node to be returned
     * @return GraphNode associated with name, null if no such node exists
     */
    public GraphNode getNodeFromName(String name){
        for ( GraphNode n : vlist ) {
            if (n.getNodeName().equalsIgnoreCase(name))
                return n;
        }
        return null;
    }

    /**
     * Return Depth First Search list of nodes on path 
     * from one Node to another.
     * @param start First node in DFS traversal
     * @param end Last node (match node) in DFS traversal
     * @return The DFS traversal from start to end node.
     */
    public List<Neighbor> DFS(String start, String end) {
         // TODO implement this method
         // may need and create a companion method
    	List<GraphNode> visitedNodes = new ArrayList<GraphNode>();
    	
    	GraphNode startNode = getNodeFromName(start);
    	GraphNode endNode = getNodeFromName(end);
    	if(endNode==null){
    		System.out.println("Incorrect vertex name");
    		return new ArrayList<Neighbor>();
    	}
    	HashMap<GraphNode, GraphNode> hm = new HashMap<GraphNode, GraphNode>();

    	hm.put(startNode, startNode);
    	
    	DFS(startNode, endNode, visitedNodes, hm);

    	List<GraphNode> backtrack = new ArrayList<GraphNode>();
    	GraphNode curr = endNode;
    	while(!curr.equals(startNode)){
    		backtrack.add(curr);
    		curr = hm.get(curr);
    	}
    	backtrack.add(startNode);
    	Collections.reverse(backtrack);
    	
    	//TYPE PROBLEM
    	List<Neighbor> path = new ArrayList<Neighbor>();
    	GraphNode lastNode = backtrack.get(0);
    	for(GraphNode c: backtrack){
    		if(c==lastNode){}
    		else{
    			try{
    				path.add(new Neighbor(lastNode.getCostTo(c.getNodeName()), c));
    				lastNode = c;
    			} catch (NotNeighborException e){
    				break;
    			}
    		}
    	}
        return path;
    }
    /* companion method for DFS */
    private void DFS(GraphNode n, GraphNode end, List<GraphNode> visitedNodes, HashMap<GraphNode, GraphNode> hm){
    	if(n.equals(end)){
    		visitedNodes.add(n);
    		//hm.put(n, value)
    		return;
    	} else{
    		visitedNodes.add(n);
    		//hm.put(n, value);
    		for(Neighbor m: n.getNeighbors()){
    			if(!visitedNodes.contains(m.getNeighborNode())){
    				hm.put(m.getNeighborNode(), n);
    				DFS(m.getNeighborNode(), end, visitedNodes, hm);
    			}
    		}
    	}
    }
    
    /**
     * OPTIONAL: Students are not required to implement Dijkstra's ALGORITHM
     *
     * Return Dijkstra's shortest path list of nodes on path 
     * from one Node to another.
     * @param start First node in path
     * @param end Last node (match node) in path
     * @return The shortest cost path from start to end node.
     */
    public List<Neighbor> Dijkstra(String start, String end){

        // TODO: implement Dijkstra's shortest path algorithm
        // may need and create a companion method
        
        return new ArrayList<Neighbor>();
    }


    /**
     * DO NOT EDIT THIS METHOD 
     * @return a random node from this graph
     */
    public GraphNode getRandomNode() {
        if (vlist.size() <= 0 ) {
            System.out.println("Must have nodes in the graph before randomly choosing one.");
            return null;
        }
        int randomNodeIndex = Game.RNG.nextInt(vlist.size());
        return vlist.get(randomNodeIndex);
    }


}
