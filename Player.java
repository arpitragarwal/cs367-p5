///////////////////////////////////////////////////////////////////////////////
// Title:            (Program 5)
// Files:            Game.java, Player.java, GraphNode.java, Spy.java,
//					 InvalidAreaFileException.java, SpyGraph.java, Neighbor.java, 
//                   Test.javaNotNeighborException.java
// Semester:         (CS 367) Spring 2016
//
// Author:           (Tone Yu, Arpit Agarwal)
// Email:            (tyu44@wisc.edu)
// CS Login:         (tone)
// Lecturer's Name:  (Skrenty)
//
////////////////////////////////////////////////////////////////////////////////
//
// Pair Partner:     (Arpit Agarwal)
// Email:            (agarwal32@wisc.edu)
// CS Login:         (arpit)
// Lecturer's Name:  (Skrenty)
////////////////////////////////////////////////////////////////////////////////
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Player {
	private String name;
	private int budget;
	private int spycams;
	private GraphNode startnode;
	private GraphNode currNode;
	private List<GraphNode> spycamLocations;

	public Player(String name, int budget, int spycams, GraphNode startnode) {
		this.name = name;
		this.budget = budget;
		this.spycams = spycams;
		this.startnode = startnode;
		this.currNode = startnode;
		this.spycamLocations = new ArrayList<GraphNode>();
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getBudget(){
		return this.budget;
	}
	
	public void decreaseBudget(int dec){
		//TODO not sure what is going on in here
		if(dec > 1){
			this.budget -= dec;
		}
	}
	
	public boolean dropSpycam(){
		if(this.spycams<=0){
			System.out.println("Not enough spycams");
			return false;
		}
		else{
			if(!currNode.getSpycam()){
				currNode.setSpycam(true);
				this.spycams -= 1;
				this.spycamLocations.add(currNode);
				System.out.println("Dropped a Spy Cam at "+ currNode.getNodeName());
				return true;
			}
			else {
				System.out.println("Already a Spy Cam there");
				return false;
			}
		}
	}
	
	public boolean pickupSpycam(GraphNode node){
		if(node.getSpycam()) {
			node.setSpycam(false);
			this.spycamLocations.remove(node);
			this.getSpycamBack(true);
			return true;
		}
		else return false;
	}
	
	public int getSpycams(){
		return this.spycams;
	}
	
	public boolean move(String name){
		//TODO NOT SURE WHAT WE NEED TO DO HERE
		//List<Neighbor> nbr_list = currNode.getNeighbors();
		Iterator<Neighbor> itr = currNode.getNeighborNames();
		boolean isNbr = false;
		while(itr.hasNext()){
			Neighbor curr = itr.next();
			if(curr.getNeighborNode().getNodeName().equals(name)) isNbr=true;
		}
		if(isNbr){
			try{
				if(budget<currNode.getCostTo(name)){
					System.out.println("Not enough money cost is "+ currNode.getCostTo(name)+" budget is "+ budget);
					return false;
				}
				this.decreaseBudget(currNode.getCostTo(name));
				this.currNode = currNode.getNeighbor(name);
			} catch (NotNeighborException e){
				return false;
			}
			return true;
		} else{
			System.out.println(name + " is not a neighbor of your current location");
			return false;
		}
	}
	
	public String getLocationName(){
		return this.currNode.getNodeName();
	}
	
	public GraphNode getLocation(){
		return this.currNode;
	}
	
	public void getSpycamBack(boolean pickupSpyCam){
		if(pickupSpyCam) {
			this.spycams +=1;
		}
	}
	
	public void printSpyCamLocations(){
		Iterator<GraphNode> itr = this.spycamLocations.iterator();
		while(itr.hasNext()){
			GraphNode curr = itr.next();
			System.out.print(curr.getNodeName()+", ");
		}
		System.out.println("");
	}
}
