///////////////////////////////////////////////////////////////////////////////
// Title:            (Program 5)
// Files:            Game.java, Player.java, GraphNode.java, Spy.java,
//					 InvalidAreaFileException.java, SpyGraph.java, Neighbor.java, 
//                   Test.javaNotNeighborException.java
// Semester:         (CS 367) Spring 2016
//
// Author:           (Tone Yu, Arpit Agarwal)
// Email:            (tyu44@wisc.edu)
// CS Login:         (tone)
// Lecturer's Name:  (Skrenty)
//
////////////////////////////////////////////////////////////////////////////////
//
// Pair Partner:     (Arpit Agarwal)
// Email:            (agarwal32@wisc.edu)
// CS Login:         (arpit)
// Lecturer's Name:  (Skrenty)
////////////////////////////////////////////////////////////////////////////////

import java.lang.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GraphNode implements Comparable<GraphNode>{
	private static int NOT_NEIGHBOR = Integer.MAX_VALUE;
	private String vertex_name;
	private List<Neighbor> neighbors;
	private boolean Spycam;
	
	//Constructor
	public GraphNode(String name){
		this.vertex_name = name;
		this.Spycam = false;
		neighbors = new ArrayList<Neighbor>();
	}
	
	public String getNodeName(){
		return this.vertex_name;
	}
	
	public List<Neighbor> getNeighbors(){
		return this.neighbors;
	}
	
	public boolean isNeighbor(String neighborName){
		//return neighbors.contains(neighborName);
		boolean found=false;
		for (Neighbor n: neighbors){
			if(n.getNeighborNode().getNodeName().equals(neighborName)) found = true;
		}
		return found;
	}
	
	public void addNeighbor(GraphNode neighbor, int cost){
		neighbors.add(new Neighbor(cost, neighbor));
		neighbors.sort(null);
	}
	
	public Iterator<Neighbor> getNeighborNames(){
		return neighbors.iterator();
	}
	
	public boolean getSpycam(){
		return this.Spycam;
	}
	
	public void setSpycam(boolean cam){
		this.Spycam = cam;
	}
	
	public int getCostTo(String neighbor_name) throws NotNeighborException{
		if(!isNeighbor(neighbor_name)) throw new NotNeighborException();

		Neighbor requestedNeighbor = null;
		Iterator<Neighbor> itr = this.getNeighborNames();
		while(itr.hasNext()){
			Neighbor curr = itr.next();
			if (curr.getNeighborNode().getNodeName().equals(neighbor_name)) {
				requestedNeighbor = curr;
				break;
			};
		}
		return requestedNeighbor.getCost();
	}
	
	public GraphNode getNeighbor(String name) throws NotNeighborException{
		if(!isNeighbor(name)) throw new NotNeighborException();
		Neighbor requestedNeighbor = null;
		Iterator<Neighbor> itr = getNeighborNames();
		Neighbor curr = null;
		while(itr.hasNext()){
			curr = itr.next();
			if(curr.getNeighborNode().getNodeName().equals(name)) {
				requestedNeighbor = curr;
				break;
			}
		}
		return requestedNeighbor.getNeighborNode();
	}
	
	public void displayCostToEachNeighbor(){
		/*Prints a list of neighbors of this GraphNode and the cost of the edge to them
 			1 b
 			4 c
 			20 d
 			1 f*/
		Iterator<Neighbor> itr = this.getNeighborNames();
		while(itr.hasNext()){
			Neighbor curr = itr.next();
			System.out.println(curr.getCost()+" "+curr.getNeighborNode().getNodeName());
		}
	}
	
	public String toString(){
		return this.vertex_name;
	}
	
	public void printNeighborNames(){
		/*Display's the node name followed by a list of neighbors to this node. Example:
		 1 b
		 4 c
		 20 d
		 1 f
		 THIS HAPPENS TO BE A DUPLICATE METHOD
		 */
		this.displayCostToEachNeighbor();
	}
	
	public int compareTo(GraphNode otherNode){
		return this.vertex_name.compareTo(otherNode.vertex_name);
	}
	
}
