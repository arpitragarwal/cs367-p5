///////////////////////////////////////////////////////////////////////////////
// Title:            (Program 5)
// Files:            Game.java, Player.java, GraphNode.java, Spy.java,
//					 InvalidAreaFileException.java, SpyGraph.java, Neighbor.java, 
//                   Test.javaNotNeighborException.java
// Semester:         (CS 367) Spring 2016
//
// Author:           (Tone Yu, Arpit Agarwal)
// Email:            (tyu44@wisc.edu)
// CS Login:         (tone)
// Lecturer's Name:  (Skrenty)
//
////////////////////////////////////////////////////////////////////////////////
//
// Pair Partner:     (Arpit Agarwal)
// Email:            (agarwal32@wisc.edu)
// CS Login:         (arpit)
// Lecturer's Name:  (Skrenty)
////////////////////////////////////////////////////////////////////////////////

public class Neighbor implements Comparable<Neighbor>{
	private GraphNode neighbor;
	private int cost;

	public Neighbor(int cost, GraphNode neighbor) {
		this.neighbor = neighbor;
		this.cost = cost;
	}
	
	public int getCost(){
		return this.cost;
	}
	
	public GraphNode getNeighborNode(){
		return this.neighbor;
	}

	public int compareTo(Neighbor otherNode){
		//TODO
		return this.neighbor.compareTo(otherNode.neighbor);
	}
	
	public String toString(){
		/*Returns a String representation of this Neighbor. 
		 * The String that is returned shows an arrow (with the cost in the middle) 
		 * and then the Neighbor node's name. Example:
 		--1--> b */
		
		return ("--" + this.cost + "--> " + this.neighbor.getNodeName());
	}
}
